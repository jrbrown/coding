module Data.FiniteSet
  ( FinSet (getCardinality, setCardinality)
  , FinSetFromInfSet (restrict)
  , finSetallElems
  , SimpleFinSet (SimpleFinSet)
  , FinInt
  , rFI
  , invElems
  , eulerF
  ) where


import Data.Algebra (Ring(..))
import Data.NumberTheory (remainder, gcdEuclid)
import Control.Applicative (Applicative(liftA2))
import Data.Maybe (mapMaybe)


-- For finite sets where their cardinality is part of their data
class FinSet a where
  getCardinality :: a -> Int
  setCardinality :: Int -> a -> a

-- For types which represent finite sets but their underlying construction uses an infinite set
class (FinSet a) => FinSetFromInfSet a where
  restrict :: a -> a  -- Ensures that our undefinedderlying element is legal


finSetallElems :: (FinSet a, Enum a) => a -> [a]
finSetallElems x =
  let m = getCardinality x
   in setCardinality m . toEnum <$> [0 .. (m - 1)]


-- First m elements from underlying type a
data SimpleFinSet a = SimpleFinSet Int a deriving(Eq, Ord, Show)

instance Functor SimpleFinSet where
  fmap f (SimpleFinSet m x) = SimpleFinSet m (f x)

instance Applicative SimpleFinSet where
  pure = SimpleFinSet 0
  (<*>) (SimpleFinSet m1 f) (SimpleFinSet m2 x) = SimpleFinSet (max m1 m2) (f x)


type FinInt = SimpleFinSet Int

rFI :: String -> FinInt
rFI str = case break (=='_') str of
            (_, "") -> error $ "Bad parse of " ++ str ++ " into FinInt"
            (x, zm) -> SimpleFinSet (read (drop 2 zm)) (read x)

instance {-# OVERLAPS #-} Show FinInt where
  show (SimpleFinSet m x) = show x ++ "_Z" ++ show m

instance Enum FinInt where
  toEnum x = SimpleFinSet x x
  fromEnum (SimpleFinSet _ x) = x

instance FinSet FinInt where
  getCardinality (SimpleFinSet m _) = m
  setCardinality m (SimpleFinSet _ x) = restrict $ SimpleFinSet m x

instance FinSetFromInfSet FinInt where
  restrict (SimpleFinSet m x) = SimpleFinSet m (x `remainder` m)

instance Ring FinInt where
  add = (restrict .) . liftA2 (+)
  neg = restrict . fmap negate
  zero = toEnum 0
  mul = (restrict .) . liftA2 (*)
  one = toEnum 1
  mrcp (SimpleFinSet m u) = case gcdEuclid m u of
                              (1, _, b) -> Just $ SimpleFinSet m (b `remainder` m)
                              (_, _, _) -> Nothing


invElems :: (FinSet a, Enum a, Ring a) => a -> [(a, a)]
invElems x' = mapMaybe calc (finSetallElems x')
  where calc x = sequence (x, mrcp x)


eulerF :: (FinSet a, Enum a, Ring a) => a -> Int
eulerF x = length $ invElems x

