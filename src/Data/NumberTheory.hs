module Data.NumberTheory
  ( intDiv
  , remainder
  , gcdEuclid
  , gcdEuclid'
  , gcdStein'
  , primes
  , Factorisation (Factorisation)
  , unwrapFact
  , fromFact
  , toPrimeFact
  , crtNtoRs
  , crtRstoN
  ) where


import Data.Map (Map, fromList, toList, empty)
import Data.List.Ordered (minus, unionAll)


intDiv :: Int -> Int -> (Int, Int)
intDiv n d
  | d > 0 = remainder' 0 n
  | d < 0 = let (q', r') = intDiv (-n) (-d)
             in (q' + 1, -r' - d)
  | otherwise = error "Div by 0"
  where remainder' q r
          | r < 0     = remainder' (q-1) (r+d)
          | r < abs d = (q, r)
          | otherwise = remainder' (q+1) (r-d)


remainder :: Int -> Int -> Int
remainder d n = snd $ intDiv d n


gcdEuclid :: Int -> Int -> (Int, Int, Int)  -- gcd, a, b
gcdEuclid n1' n2' = calc n1' n2' 1 0 0 1
  where
    calc :: Int -> Int -> Int -> Int -> Int -> Int -> (Int, Int, Int)
    calc n1 n2 a1 b1 a2 b2
      | n2 == 0          = (n1, a1, b1)
      | n1 < 0           = calc (-n1) n2 (-a1) b1 (-a2) b2
      | n2 < 0           = calc n1 (-n2) a1 (-b1) a2 (-b2)
      | n2 > n1          = calc n2 n1 a2 b2 a1 b1
      | otherwise        = let (q, r) = intDiv n1 n2
                            in calc n2 r a2 b2 (a1 - q*a2) (b1 - q*b2)


-- Implementation provided for interest / reference / debugging
gcdEuclid' :: Int -> Int -> Int
gcdEuclid' n1 n2
  | n2 == 0          = n1
  | n1 < 0 || n2 < 0 = gcdEuclid' (abs n1) (abs n2)
  | n2 > n1          = gcdEuclid' n2 n1
  | otherwise        = gcdEuclid' n2 (remainder n1 n2)


gcdStein' :: Int -> Int -> Int
gcdStein' n1 n2
  | n2 == 0           = n1
  | n1 < 0 || n2 < 0  = gcdStein' (abs n1) (abs n2)
  | n2 > n1           = gcdStein' n2 n1
  | odd n1 && odd n2  = gcdStein' ((n1 - n2) `div` 2) n2
  | even n1 && odd n2 = gcdStein' (n1 `div` 2) n2
  | odd n1 && even n2 = gcdStein' n1 (n2 `div` 2)
  | otherwise         = 2 * gcdStein' (n1 `div` 2) (n2 `div` 2)


primes :: [Int]
primes = 2 : 3 : [5,7..] `minus` unionAll [[p*p, p*p + 2*p ..] | p <- tail primes]


newtype Factorisation = Factorisation (Map Int Int) deriving (Eq, Show) 


unwrapFact :: Factorisation -> Map Int Int
unwrapFact (Factorisation x) = x


fromFact :: Factorisation -> Int
fromFact = product . fmap (uncurry (^)) . toList . unwrapFact


toPrimeFact :: Int -> Factorisation
toPrimeFact x' = calc [] x' 2 0 (tail primes)
  where
    calc :: [(Int, Int)] -> Int -> Int -> Int -> [Int] -> Factorisation
    calc _ _ _ _ []      = error "Run out of primes :("
    calc _ 0 _ _ _       = Factorisation empty  -- Maybe throw error here?
    calc acc 1 p count _ = (Factorisation . fromList . filter ((/=) 0 . snd)) ((p, count) : acc)
    calc acc x p count ps@(next_p:next_ps)
      | x `mod` p /= 0   = calc ((p, count) : acc) x next_p 0 next_ps
      | otherwise        = calc acc (x `div` p) p (count + 1) ps


crtNtoRs :: [Int] -> Int -> [Int]
crtNtoRs ms n = remainder n <$> ms


crtRstoN :: [Int] -> [Int] -> Int
crtRstoN ms rs = (flip remainder m . sum) $ calc <$> zip ms rs
  where
    m = product ms
    calc (mj, rj) =
      let uj = m `div` mj
          (_, _, bj) = gcdEuclid mj uj
       in rj * bj * uj

