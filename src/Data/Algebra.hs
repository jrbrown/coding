{-# LANGUAGE UndecidableInstances #-}
{-# LANGUAGE FlexibleInstances #-}

module Data.Algebra
  ( Group (..)
  , Ring (..)
  , Field (..)
  ) where


class Monoid a => Group a where
  inv :: a -> a


class Ring a where
  add :: a -> a -> a
  neg :: a -> a
  zero :: a
  mul  :: a -> a -> a
  mdiv :: a -> a -> Maybe a
  mdiv x y = mul x <$> mrcp y
  mrcp :: a -> Maybe a
  mrcp = mdiv one
  one :: a
  {-# MINIMAL add, neg, zero, mul, (mdiv | mrcp), one #-}


instance Ring Float where
  add = (+)
  neg = negate
  zero = 0
  mul = (*)
  mrcp = Just . recip
  one = 1

instance Ring Double where
  add = (+)
  neg = negate
  zero = 0
  mul = (*)
  mrcp = Just . recip
  one = 1

instance Ring Int where
  add = (+)
  neg = negate
  zero = 0
  mul = (*)
  mdiv x y = if x `mod` y == 0 then Just (x `div` y) else Nothing
  one = 1

instance Ring Integer where
  add = (+)
  neg = negate
  zero = 0
  mul = (*)
  mdiv x y = if x `mod` y == 0 then Just (x `div` y) else Nothing
  one = 1


class (Ring a) => Field a where
  rcp :: a -> a
  rcp = fdiv one
  fdiv :: a -> a -> a
  fdiv x y = mul x (rcp y)
  {-# MINIMAL rcp | fdiv #-}


instance Field Float where
  rcp = recip

instance Field Double where
  rcp = recip

